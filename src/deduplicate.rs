use std::fs::File;
use std::io;
use std::os::unix::io::AsRawFd;
use std::ptr::null;
use std::thread::ScopedJoinHandle;
use std::time::Instant;

use crossbeam::channel::{Receiver, Sender};
use libc::{ioctl, perror};

use crate::file_lister::FileNameItem;
use crate::{logger, Cmd};

const MAX_FILES_PER_DEDUPE: usize = 16;
pub const DEDUPE_SIZE: u64 = 1 << 24; // 16MB
const FS_IOC_FIEMAP: u64 = 0xc020660b;
const FIDEDUPERANGE: u64 = 0xc0189436;
const FILE_DEDUPE_RANGE_SAME: i32 = 0;
const FILE_DEDUPE_RANGE_DIFFERS: i32 = 1;

#[repr(C)]
#[derive(Default)]
struct FileDedupeRange {
	src_offset: u64,
	src_length: u64,
	dest_count: u16,
	reserved1: u16,
	reserved2: u32,
	file_dedupe_range_info: [FileDedupeRangeInfo; MAX_FILES_PER_DEDUPE],
}

#[repr(C)]
#[derive(Copy, Clone, Default)]
struct FileDedupeRangeInfo {
	dest_fd: i64,
	dest_offset: u64,
	bytes_deduped: u64,
	status: i32,
	reserved: u32,
}

pub struct DeDupeRepr<'a> {
	pub thread: ScopedJoinHandle<'a, ()>,
	pub sender: Sender<(Vec<FileNameItem>, u64)>,
}

#[derive(Debug)]
pub enum Error {
	IoError(io::Error),
	UnknownError(i32),
}

pub fn deduplicator(
	receiver: Receiver<(Vec<FileNameItem>, u64)>,
	logger: &Sender<logger::Command>,
	thread_id: u64,
	settings: &Cmd,
) {
	for item in receiver {
		logger.send(logger::Command::DeduplicateStart { time: Instant::now() }).unwrap();
		deduplicate(item, logger, thread_id, settings);
	}
}

const MAX_FIEMAP_RETURN_EXTENTS: usize = 16;
#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct FiemapRequest {
	fm_start: u64,
	fm_length: u64,
	fm_flags: u32,
	fm_mapped_extents: u32,
	fm_extent_count: u32,
	fm_reserved: u32,
	fm_extents: [FiemapResult; MAX_FIEMAP_RETURN_EXTENTS],
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct FiemapResult {
	fe_logical: u64,
	fe_physical: u64,
	fe_length: u64,
	fe_reserved64: [u64; 2],
	fe_flags: u32,
	fe_reserved: [u32; 3],
}

pub const fn prepare_fiemap_results() -> [FiemapResult; MAX_FIEMAP_RETURN_EXTENTS] {
	[FiemapResult {
		fe_logical: 0,
		fe_physical: 0,
		fe_length: 0,
		fe_reserved64: [0, 0],
		fe_flags: 0,
		fe_reserved: [0, 0, 0],
	}; MAX_FIEMAP_RETURN_EXTENTS]
}

pub fn get_fiemap(
	file: &File,
	file_len: u64,
	_logger_tx: &Sender<logger::Command>,
	_thread_id: u64,
) -> FiemapRequest {
	let request = FiemapRequest {
		fm_start: 0,
		fm_length: file_len,
		fm_flags: 0,
		fm_mapped_extents: 0,
		fm_extent_count: MAX_FIEMAP_RETURN_EXTENTS as u32,
		fm_reserved: 0,
		fm_extents: prepare_fiemap_results(),
	};
	let result = unsafe { ioctl(file.as_raw_fd(), FS_IOC_FIEMAP, &request) };
	assert_eq!(result, 0, "fiemap result not zero"); // TODO: Proper error
	request
}

pub fn deduplicate(
	files: (Vec<FileNameItem>, u64),
	logger_tx: &Sender<logger::Command>,
	thread_id: u64,
	settings: &Cmd,
) {
	let mut open_files = Vec::<File>::new();
	let mut main_file_len = 0;

	let (files, len) = files;

	// Open the files
	for filename in files {
		main_file_len = len;
		let file = match File::open(&filename.full_path()) {
			Ok(v) => v,
			Err(e) => {
				logger_tx
					.send(logger::Command::DeduplicateError {
						context: Some(filename.full_path().to_string_lossy().to_string()),
						error: Error::IoError(e),
						id: thread_id,
					})
					.unwrap();
				continue;
			}
		};

		open_files.push(file);
	}

	if open_files.len() < 2 {
		return; // TODO: Return info about this?
	}

	// Get main file info
	let main_file = open_files.pop().unwrap();
	let main_file_fiemap = get_fiemap(&main_file, main_file_len, logger_tx, thread_id);

	while !open_files.is_empty() {
		let mut used_files = Vec::new(); // Prevent rust from closing the files prematurely

		// Only dedupe in MAX_FILES_PER_DEDUPE chunks
		let mut i = 0;
		let mut file_infos = [FileDedupeRangeInfo::default(); MAX_FILES_PER_DEDUPE];

		// Construct dedupe destinations
		while let Some(file) = open_files.pop() {
			if !settings.force_rededup {
				// First check if files already deduped
				let file_fiemap = get_fiemap(&file, main_file_len, logger_tx, thread_id);

				let mut same = true;

				if file_fiemap.fm_mapped_extents == main_file_fiemap.fm_mapped_extents {
					for i in 0..file_fiemap.fm_mapped_extents {
						if main_file_fiemap.fm_extents[i as usize].fe_physical
							!= file_fiemap.fm_extents[i as usize].fe_physical
							|| main_file_fiemap.fm_extents[i as usize].fe_logical
								!= file_fiemap.fm_extents[i as usize].fe_logical
							|| main_file_fiemap.fm_extents[i as usize].fe_length
								!= file_fiemap.fm_extents[i as usize].fe_length
						{
							same = false;
							break;
						}
					}
				}

				if same {
					// Already deduped
					// TODO: Maybe log some info on how much was already deduplicated
					continue;
				}
			}

			file_infos[i].dest_fd = i64::from(file.as_raw_fd());

			used_files.push(file);

			i += 1;
			if i >= MAX_FILES_PER_DEDUPE {
				// Only dedupe in MAX_FILES_PER_DEDUPE chunks
				// TODO: Confirm that the rest actually get deduplicated if we hit this
				break;
			}
		}

		// Deduplicate
		if i > 0 {
			let mut remaining_len = main_file_len;
			let mut offset = 0;

			while remaining_len != 0 {
				let dedup_size =
					if remaining_len > DEDUPE_SIZE { DEDUPE_SIZE } else { remaining_len };

				let mut this_file_infos = file_infos;
				for file_info in &mut this_file_infos {
					file_info.dest_offset = offset;
				}

				let r = FileDedupeRange {
					src_offset: offset,
					src_length: dedup_size,
					dest_count: i as u16,
					reserved1: 0,
					reserved2: 0,
					file_dedupe_range_info: this_file_infos,
				};

				remaining_len -= dedup_size;
				offset += dedup_size;
				unsafe {
					let result = ioctl(main_file.as_raw_fd(), FIDEDUPERANGE, &r);

					if result == -1 {
						perror(null());
					}
				}

				for (j, arg) in r.file_dedupe_range_info.into_iter().enumerate() {
					if j >= i {
						break;
					}

					match arg.status {
						FILE_DEDUPE_RANGE_SAME => {}
						FILE_DEDUPE_RANGE_DIFFERS => {}
						-22 => {} // Already deduped or something, idk tbh. EINVAL
						v => {
							logger_tx
								.send(logger::Command::DeduplicateError {
									context: Some(format!("{main_file:?} {used_files:?}")),
									error: Error::UnknownError(v),
									id: thread_id,
								})
								.unwrap();
						}
					}
				}
			}

			logger_tx
				.send(logger::Command::Deduplicated {
					files: used_files.len(),
					size: main_file_len,
				})
				.unwrap();
		}

		drop(used_files);
	}

	drop(main_file);
}
