use std::collections::{BTreeMap, HashMap};
use std::ffi::{OsStr, OsString};
use std::fs::read_dir;
use std::hash::Hasher;
use std::io;
use std::os::unix::ffi::OsStringExt;
use std::os::unix::fs::MetadataExt;
use std::path::MAIN_SEPARATOR;
use std::sync::atomic::Ordering::{AcqRel, Acquire, Release};
use std::sync::atomic::{AtomicBool, AtomicIsize};
use std::sync::Arc;
use std::time::{Duration, Instant};

use crossbeam::channel::Sender;
use parking_lot::Mutex;
use seahash::SeaHasher;

use crate::file_reader::Command;
use crate::{logger, Cmd};

#[derive(Debug)]
pub struct FilePathItem {
	path: Option<Arc<FilePathItem>>,
	name: Box<OsStr>,
}

impl FilePathItem {
	pub fn new(name: OsString, path: Option<Arc<Self>>) -> Self {
		// Strip / from components
		let name_buf: Vec<u8> =
			name.into_vec().into_iter().filter(|c| *c != MAIN_SEPARATOR as u8).collect();

		Self { path, name: OsString::from_vec(name_buf).into_boxed_os_str() }
	}

	pub fn full_path(&self) -> OsString {
		let sep = MAIN_SEPARATOR.to_string();
		let components = self.components();
		let mut p =
			OsString::with_capacity(components.iter().fold(0, |acc, c| acc + c.name.len() + 1));

		#[cfg(debug_assertions)]
		let xcap = p.capacity();

		for component in components {
			p.push(&component.name);
			p.push(&sep);
		}

		#[cfg(debug_assertions)]
		{
			debug_assert!(xcap == p.len());
			debug_assert!(xcap == p.capacity());
		}

		p
	}

	fn components(&self) -> Vec<&Self> {
		if self.path.is_some() {
			let mut components = self.path.as_ref().unwrap().components();
			components.push(self);
			components
		} else {
			vec![self]
		}
	}
}

#[derive(Debug)]
pub struct FileNameItem {
	path: Arc<FilePathItem>,
	name: Box<OsStr>,
}

impl FileNameItem {
	pub fn new(name: OsString, path: Arc<FilePathItem>) -> Self {
		Self { path, name: name.into_boxed_os_str() }
	}

	pub fn full_path(&self) -> OsString {
		let sep = MAIN_SEPARATOR.to_string();
		let components = self.path.components();
		let mut p = OsString::with_capacity(
			components.iter().fold(0, |acc, c| acc + c.name.len() + 1) + self.name.len(),
		);

		#[cfg(debug_assertions)]
		let xcap = p.capacity();

		for component in components {
			p.push(&component.name);
			p.push(&sep);
		}

		p.push(&self.name);

		#[cfg(debug_assertions)]
		{
			debug_assert!(xcap == p.len());
			debug_assert!(xcap == p.capacity());
		}

		p
	}
}

#[derive(Debug)]
pub enum FileListError {
	IoError(io::Error),
}

pub struct FileLister<'a> {
	pub logger: Sender<logger::Command>,
	pub thread_id: u64,
	pub dev: Option<u64>,
	pub list_queue: (flume::Sender<Arc<FilePathItem>>, flume::Receiver<Arc<FilePathItem>>),
	pub total_working: &'a AtomicIsize,
	pub reading_started: &'a AtomicBool,
	pub read_queue: (flume::Sender<Command>, flume::Receiver<Command>),
	pub found_filesizes: Arc<Mutex<HashMap<u64, Option<FileNameItem>>>>,
	pub precache: Option<Arc<Mutex<BTreeMap<u64, (u64, u64)>>>>,
	pub hashmapper: Sender<(u64, FileNameItem, u64)>,
}

impl<'a> FileLister<'a> {
	/**
	Recursively do stuff
	**/
	fn dir_descend(&self, path: &Arc<FilePathItem>, settings: &Cmd) {
		let full_file_name = path.full_path();

		let read = match read_dir(full_file_name) {
			Ok(v) => v,
			Err(e) => {
				self.logger
					.send(logger::Command::FileListError {
						context: path.clone(),
						error: FileListError::IoError(e),
					})
					.unwrap();
				return;
			}
		};

		for entry in read {
			let entry = match entry {
				Ok(v) => v,
				Err(e) => {
					self.logger
						.send(logger::Command::FileListError {
							context: path.clone(),
							error: FileListError::IoError(e),
						})
						.unwrap();
					continue;
				}
			};

			let meta = match entry.metadata() {
				Ok(v) => v,
				Err(e) => {
					self.logger
						.send(logger::Command::FileListError {
							context: path.clone(),
							error: FileListError::IoError(e),
						})
						.unwrap();
					continue;
				}
			};

			// If the file/dir resides on a different filesystem, don't touch it.
			// Applying this check on all files rather than just dirs seems to cause it to go from 2.5s to scan all files to 6s

			if self.dev.is_none() || (self.dev.is_some() && meta.dev() == self.dev.unwrap()) {
				// It's a dir, recurse
				if meta.is_dir() {
					let new_path =
						Arc::new(FilePathItem::new(entry.file_name(), Some(path.clone())));

					let (list_send, _) = &self.list_queue;
					list_send.send(new_path).unwrap();
				} else if meta.is_file() {
					// Do something
					let len = meta.len();

					if len < settings.min_file_size || len > settings.max_file_size {
						continue;
					}

					let (send, _) = &self.read_queue;

					let file_name = FileNameItem::new(entry.file_name(), path.clone());

					// Check if this file was already hashed and cached
					if let Some(precache) = &self.precache {
						let mut fnhasher = SeaHasher::new();
						fnhasher.write(&file_name.full_path().into_vec());
						let fnhash = fnhasher.finish();

						let mut locked = precache.lock();
						if let Some((hash, cache_len)) = locked.remove(&fnhash) {
							if len == cache_len {
								// It was cached!
								self.logger
									.send(logger::Command::SkipProcessableFile { size: len })
									.unwrap();
								self.hashmapper.send((hash, file_name, len)).unwrap();
								drop(locked);

								let mut found_map = self.found_filesizes.lock();

								if let Some(other) = found_map.get(&len) {
									// We previously encountered a file with this size, but we haven't processed it yet, process it
									if other.is_some() {
										let other = found_map.remove(&len).unwrap().unwrap();
										self.logger
											.send(logger::Command::ProcessableFile { size: len })
											.unwrap();
										found_map.insert(len, None);

										send.send(Command::NewFile(other, len)).unwrap();
									}
								} else {
									// Haven't seen a file of this size yet, but remember that we did, but also don't process it later
									found_map.insert(len, None);
								}
								drop(found_map);

								continue;
							}
						}
						drop(locked);
					}

					// Put filesizes/filenames into a hashmap, to avoid reading files that don't have other files of the matching size
					// Once a file of matching size is found, queue for reading

					let mut found_map = self.found_filesizes.lock();

					if let Some(other) = found_map.get(&len) {
						// We previously encountered a file with this size, but we haven't processed it yet, do it now, and remember that we did so
						if other.is_some() {
							let other = found_map.remove(&len).unwrap().unwrap();
							self.logger
								.send(logger::Command::ProcessableFile { size: len })
								.unwrap();
							send.send(Command::NewFile(other, len)).unwrap();
							found_map.insert(len, None);
						}

						// We've seen this size before, we know there's multiple files of this size at this point, process this one immediately
						self.logger.send(logger::Command::ProcessableFile { size: len }).unwrap();
						send.send(Command::NewFile(file_name, len)).unwrap();
					} else {
						// Haven't seen a file of this size yet, but remember that we did
						found_map.insert(len, Some(file_name));
					}
				}
			}
		}
	}

	/// Generates a list of files, sends to threads
	pub fn list_files(&self, settings: &Cmd) {
		let (send, _) = &self.read_queue;
		let (_, list_recv) = &self.list_queue;

		loop {
			let path = list_recv.recv_timeout(Duration::from_millis(10));

			if let Ok(path) = path {
				// There was something on the queue, process
				self.total_working.fetch_add(1, AcqRel);

				// Do stuff
				self.dir_descend(&path, settings);

				self.total_working.fetch_sub(1, AcqRel);
			} else {
				// There was nothing on the queue
				if self.total_working.load(Acquire) == 0 {
					// There was nothing on the queue, and also nothing doing any work, exit
					self.reading_started.store(true, Release);
					break;
				}
			}
		}

		send.send(Command::Finished).unwrap();

		self.logger
			.send(logger::Command::FileListerExit { time: Instant::now(), id: self.thread_id })
			.unwrap();
	}
}
