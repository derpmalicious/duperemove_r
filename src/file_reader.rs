use std::fs::File;
use std::hash::Hasher;
use std::io::{self, ErrorKind, Read};
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering::Relaxed;
use std::thread::sleep;
use std::time::{Duration, Instant};

use crossbeam::channel::Sender;
use seahash::SeaHasher;

use crate::file_lister::FileNameItem;
use crate::{file_reader, logger, Cmd, READ_BUFSIZE};

pub enum Command {
	NewFile(FileNameItem, u64),
	Finished,
}

#[derive(Debug)]
pub struct FileInvalidSize {
	pub expected: u64,
	pub read: u64,
}

#[derive(Debug)]
pub enum FileReadError {
	FileInvalidSize(FileInvalidSize),
	IoError(io::Error),
}

pub struct FileReader<'a> {
	pub logger: Sender<logger::Command>,
	pub hashmapper: Sender<(u64, FileNameItem, u64)>,
	pub thread_id: u64,
	pub read_queue: (flume::Sender<Command>, flume::Receiver<Command>),
	pub reading_started: &'a AtomicBool,
}

impl<'a> FileReader<'a> {
	/** Reads files, hashes, forwards to hashmapper **/
	pub fn file_reader_thread(&self, settings: &Cmd) {
		// TODO: Get rid of spinlock
		loop {
			sleep(Duration::from_millis(1));
			if self.reading_started.load(Relaxed) {
				break;
			}
		}
		self.logger.send(logger::Command::ReadStart { time: Instant::now() }).unwrap();

		// Do stuff
		let mut buf = vec![0u8; READ_BUFSIZE as usize];

		let (send, rcv) = &self.read_queue;

		for item in rcv {
			match item {
				file_reader::Command::NewFile(filenameitem, expected_size) => {
					let filename = filenameitem.full_path();

					// Open the file
					let mut file = match File::open(&filename) {
						Ok(v) => v,
						Err(e) => {
							self.logger
								.send(logger::Command::FileReadError {
									context: filename,
									error: FileReadError::IoError(e),
								})
								.unwrap();
							continue;
						}
					};

					let mut fhasher = SeaHasher::new();
					let mut read_total = 0;
					let mut read_error = None;

					if settings.partial_hash {
						// Read once
						let result = file.read_exact(&mut buf);

						drop(file);

						if result.is_ok() {
							read_total = expected_size as usize;
							fhasher.write(&buf[0..READ_BUFSIZE as usize]);
						} else {
							let e = result.err().unwrap();
							if e.kind() != ErrorKind::UnexpectedEof {
								read_error = Some(e);
							}
						}
					} else {
						// Read until the file has ended
						loop {
							let result = file.read(&mut buf);

							let nread = match result {
								Ok(0) => break,
								Ok(nread) => nread,
								Err(e) => {
									read_error = Some(e);
									break;
								}
							};

							read_total += nread;
							fhasher.write(&buf[0..nread]);
						}

						drop(file);
					}

					if let Some(e) = read_error {
						self.logger
							.send(logger::Command::FileReadError {
								context: filename,
								error: FileReadError::IoError(e),
							})
							.unwrap();
					} else if expected_size != read_total as u64 {
						self.logger
							.send(logger::Command::FileReadError {
								context: filename,
								error: FileReadError::FileInvalidSize(FileInvalidSize {
									expected: expected_size,
									read: read_total as u64,
								}),
							})
							.unwrap();
					} else {
						let hash = fhasher.finish();
						self.hashmapper.send((hash, filenameitem, expected_size)).unwrap();
					}

					self.logger.send(logger::Command::DataRead { len: read_total }).unwrap();
				}
				file_reader::Command::Finished => {
					self.logger
						.send(logger::Command::IoThreadExit {
							id: self.thread_id,
							time: Instant::now(),
						})
						.unwrap();

					send.send(file_reader::Command::Finished).unwrap(); // Put the command back for the other threads

					return;
				}
			}
		}
	}
}
