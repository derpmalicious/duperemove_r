use std::collections::BTreeMap;
use std::time::Instant;

use crossbeam::channel::{Receiver, Sender};

use crate::file_lister::FileNameItem;
use crate::logger;

/// Puts stuff into a hashmap
pub fn hashmapper(
	receiver: Receiver<(u64, FileNameItem, u64)>,
	logger: &Sender<logger::Command>,
) -> (BTreeMap<u64, (FileNameItem, u64)>, BTreeMap<(u64, u64), Vec<FileNameItem>>) {
	let mut onemap = BTreeMap::new();
	let mut nmap = BTreeMap::new();

	for (hash, item, len) in receiver {
		if let Some((one_item, len)) = onemap.remove(&hash) {
			nmap.insert((hash, len), vec![one_item, item]);
		} else if let Some(n_item) = nmap.get_mut(&(hash, len)) {
			n_item.push(item);
		} else {
			onemap.insert(hash, (item, len));
		}
	}

	logger.send(logger::Command::HashmapperExit { time: Instant::now() }).unwrap();

	(onemap, nmap)
}
