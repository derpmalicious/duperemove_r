use std::ffi::OsString;
use std::sync::Arc;
use std::time::Instant;

use crossbeam::channel::Receiver;
use humansize::{format_size, BINARY};

use crate::deduplicate::Error;
use crate::file_lister::FileListError;
use crate::file_reader::FileReadError;
use crate::{deduplicate, Cmd, FilePathItem};

const STATUS_INTERVAL: u32 = 100;

pub enum Command {
	// Messages
	Message { message: String },

	// Thread Debug stuff
	FileListerExit { time: Instant, id: u64 },
	HashmapperExit { time: Instant },
	IoThreadExit { id: u64, time: Instant },

	// Time statistics
	ListingStart { time: Instant },
	ListingEnd { time: Instant },
	ReadStart { time: Instant },
	ReadEnd { time: Instant },
	DeduplicateStart { time: Instant },
	DeduplicateEnd { time: Instant },

	// Progress statistics
	Deduplicated { files: usize, size: u64 },
	ProcessableFile { size: u64 },
	SkipProcessableFile { size: u64 },
	DataRead { len: usize },
	EndReport { total_hashes: usize, hashes_with_duplicates: usize, total_duplicate_hashes: usize },

	// Errors
	FileListError { context: Arc<FilePathItem>, error: FileListError },
	FileReadError { context: OsString, error: FileReadError },
	DeduplicateError { context: Option<String>, error: deduplicate::Error, id: u64 },
}

fn print_listing_status(start: Option<Instant>, file_count: u64, file_size: u64) {
	let start = start.unwrap_or_else(Instant::now);

	let elapsed = start.elapsed().as_secs();
	let elapsed = if elapsed > 0 { elapsed } else { 1 };

	eprint!(
		"\rFound {} files totalling {} ({} files per second)",
		file_count,
		format_size(file_size, BINARY),
		file_count / elapsed
	);
}

fn print_read_status(
	data_read: usize,
	files_read: u64,
	read_start: Option<Instant>,
	processable_file_counter: u64,
	processable_file_size: u64,
) {
	let read_start = read_start.unwrap_or_else(Instant::now);

	let elapsed = read_start.elapsed().as_secs();
	let elapsed = if elapsed > 0 { elapsed } else { 1 };

	eprint!(
		"\rRead {} of {} ({}ps) in {} of {} files          ",
		format_size(data_read, BINARY),
		format_size(processable_file_size, BINARY),
		format_size(data_read as u64 / elapsed, BINARY),
		files_read,
		processable_file_counter,
	);
}

fn print_deduplicate_status(files: usize, size: u64, deduplicate_start: Option<Instant>) {
	let deduplicate_start = deduplicate_start.unwrap_or_else(Instant::now);

	let elapsed = deduplicate_start.elapsed().as_secs();
	let elapsed = if elapsed > 0 { elapsed } else { 1 };

	eprint!(
		"\rDeduplicated {} ({}ps) in {} files          ",
		format_size(size, BINARY),
		format_size(size / elapsed, BINARY),
		files
	);
}

/// Logs stuff
pub fn logger(receiver: Receiver<Command>, start_time: Instant, settings: &Cmd) {
	let mut data_read_total = 0;
	let mut files_read_total = 0;
	let mut deduplicated_files = 0;
	let mut deduplicated_size = 0;
	let mut processable_file_counter = 0;
	let mut processable_file_size = 0;
	let mut skipped_file_size = 0;
	let mut skipped_file_counter = 0;

	let mut read_start: Option<Instant> = None;
	let mut listing_start: Option<Instant> = None;
	let mut deduplicate_start: Option<Instant> = None;

	let mut last_status_print = Instant::now();

	let mut total_hashes_v: usize = 0;
	let mut hashes_with_duplicates_v: usize = 0;
	let mut total_duplicate_hashes_v: usize = 0;

	for item in receiver {
		match item {
			Command::Message { message } => {
				if settings.verbose {
					eprintln!("{message}");
				}
			}
			Command::FileListError { context, error } => {
				if !settings.quiet {
					match error {
						FileListError::IoError(error) => {
							eprintln!("Error listing files in {:?}: {error}", context.full_path());
						}
					}
				}
			}
			Command::FileReadError { context, error } => {
				if !settings.quiet {
					match error {
						FileReadError::IoError(error) => {
							eprintln!("Error reading file {context:?}: {error}");
						}
						FileReadError::FileInvalidSize(fis) => {
							eprintln!(
								"File {:?} has an unexpected size. Expected {}, got {}",
								context, fis.expected, fis.read
							);
						}
					}
				}
			}
			Command::DataRead { len } => {
				data_read_total += len;
				files_read_total += 1;

				if !settings.no_progress
					&& last_status_print.elapsed().as_millis() > u128::from(STATUS_INTERVAL)
				{
					last_status_print = Instant::now();
					print_read_status(
						data_read_total,
						files_read_total,
						read_start,
						processable_file_counter,
						processable_file_size,
					);
				}
			}
			Command::IoThreadExit { id, time } => {
				if settings.thread_verbose {
					eprintln!(
						"Io thread {id} exiting (Done in {}ms)",
						time.duration_since(start_time).as_millis()
					);
				}
			}
			Command::FileListerExit { time, id } => {
				if settings.thread_verbose {
					eprintln!(
						"File lister thread {id} exiting (Done in {}ms)",
						time.duration_since(start_time).as_millis()
					);
				}
			}
			Command::HashmapperExit { time } => {
				if settings.thread_verbose {
					eprintln!(
						"Hashmapper exiting (Done in {}ms)",
						time.duration_since(start_time).as_millis()
					);
				}
			}
			Command::DeduplicateError { error, context, id: _ } => {
				if !settings.quiet {
					match error {
						deduplicate::Error::IoError(error) => match context {
							None => {
								eprintln!("{error}");
							}
							Some(context) => {
								eprintln!("{context}: {error}");
							}
						},
						Error::UnknownError(code) => match context {
							None => {
								eprintln!("Unknown deduplicate status returned: {code}",);
							}
							Some(v) => {
								eprintln!("{v}: unknown deduplicate status returned: {code}");
							}
						},
					}
				}
			}
			Command::ReadStart { time } => {
				if read_start.is_none() {
					read_start = Some(time);
				}
			}
			Command::ReadEnd { time } => {
				if settings.verbose {
					if let Some(read_start) = read_start {
						eprintln!(
							"File reading finished in {}ms",
							time.duration_since(read_start).as_millis()
						);
					} else {
						eprintln!("File reading finished before it started");
					}
				}
			}
			Command::EndReport { total_hashes, hashes_with_duplicates, total_duplicate_hashes } => {
				total_hashes_v = total_hashes;
				hashes_with_duplicates_v = hashes_with_duplicates;
				total_duplicate_hashes_v = total_duplicate_hashes;
			}
			Command::Deduplicated { files, size } => {
				deduplicated_files += files;
				deduplicated_size += files as u64 * size;

				if !settings.no_progress
					&& last_status_print.elapsed().as_millis() > u128::from(STATUS_INTERVAL)
					&& !settings.dry_run
				{
					last_status_print = Instant::now();
					print_deduplicate_status(
						deduplicated_files,
						deduplicated_size,
						deduplicate_start,
					);
				}
			}
			Command::DeduplicateStart { time } => {
				// Only record the first submission
				if deduplicate_start.is_none() {
					deduplicate_start = Some(time);
				}
			}
			Command::DeduplicateEnd { time } => {
				if settings.verbose {
					if let Some(deduplicate_start) = deduplicate_start {
						eprintln!(
							"Deduplication finished in {}ms",
							time.duration_since(deduplicate_start).as_millis()
						);
					} else {
						eprintln!("Deduplication finished before it started");
					}
				}
			}
			Command::ListingStart { time } => {
				listing_start = Some(time);
			}
			Command::ListingEnd { time } => {
				if settings.verbose {
					if let Some(listing_start) = listing_start {
						eprintln!(
							"File listing finished in {}ms",
							time.duration_since(listing_start).as_millis()
						);
					} else {
						eprintln!("File listing finished before it started");
					}
				}
			}
			Command::ProcessableFile { size } => {
				processable_file_counter += 1;
				processable_file_size += size;
				if !settings.no_progress
					&& settings.list_before_reading
					&& last_status_print.elapsed().as_millis() > u128::from(STATUS_INTERVAL)
					&& read_start.is_none()
				{
					last_status_print = Instant::now();
					print_listing_status(
						listing_start,
						processable_file_counter + skipped_file_counter,
						processable_file_size + skipped_file_size,
					);
				}
			}
			Command::SkipProcessableFile { size } => {
				skipped_file_counter += 1;
				skipped_file_size += size;
				if !settings.no_progress
					&& settings.list_before_reading
					&& last_status_print.elapsed().as_millis() > u128::from(STATUS_INTERVAL)
					&& read_start.is_none()
				{
					last_status_print = Instant::now();
					print_listing_status(
						listing_start,
						processable_file_counter + skipped_file_counter,
						processable_file_size + skipped_file_size,
					);
				}
			}
		};
	}

	if !settings.no_progress {
		eprintln!();

		let read_time_total = read_start.map_or(0, |v| v.elapsed().as_secs());
		let time_total = start_time.elapsed();

		if read_time_total > 0 {
			eprintln!(
				"Total files read: {files_read_total} of {}, Total data read: {} of {} ({}ps average)",
				processable_file_counter + skipped_file_counter,
				format_size(data_read_total, BINARY),
				format_size(processable_file_size + skipped_file_size, BINARY),
				format_size(data_read_total as u64 / read_time_total, BINARY)
			);
		} else {
			eprintln!(
				"Total files read: {files_read_total}, Total data read: {} (Very fast)",
				format_size(data_read_total, BINARY),
			);
			eprintln!(
				"Total files read: {files_read_total} of {}, Total data read: {} of {} (Very fast)",
				processable_file_counter + skipped_file_counter,
				format_size(data_read_total, BINARY),
				format_size(processable_file_size + skipped_file_size, BINARY),
			);
		}

		eprintln!(
			"Total hashes found: {total_hashes_v}, Hashes with duplicates: {hashes_with_duplicates_v}, Total duplicate hashes: {total_duplicate_hashes_v}",
		);
		if settings.dry_run {
			eprintln!(
				"Deduplicatable: {deduplicated_files} files containing {} total",
				format_size(deduplicated_size, BINARY),
			);
		} else {
			eprintln!(
				"Deduplicated {deduplicated_files} files containing {} total",
				format_size(deduplicated_size, BINARY),
			);
		}

		eprintln!("Total time elapsed: {}ms", time_total.as_millis());
	}
}
