mod deduplicate;
mod file_lister;
mod file_reader;
mod hashmapper;
mod logger;

use std::collections::{BTreeMap, HashMap};
use std::ffi::OsString;
use std::fs::{metadata, File};
use std::hash::Hasher;
use std::io::{stdout, Write};
use std::os::unix::ffi::{OsStrExt, OsStringExt};
use std::os::unix::fs::MetadataExt;
use std::path::{Path, MAIN_SEPARATOR};
use std::sync::atomic::{AtomicBool, AtomicIsize};
use std::sync::Arc;
use std::time::Instant;
use std::{io, thread};

use clap::{arg, command, Parser};
use crossbeam::channel::unbounded;
use humansize::{format_size, BINARY};
use parking_lot::Mutex;
use seahash::SeaHasher;
use serde::{Deserialize, Serialize};

use crate::deduplicate::{deduplicator, DeDupeRepr};
use crate::file_lister::{FileLister, FileNameItem, FilePathItem};
use crate::file_reader::FileReader;
use crate::hashmapper::hashmapper;

const THREAD_STACK_SIZE: usize = 4 * 1024;
const READ_BUFSIZE: usize = 4 * 1024;

#[derive(Debug)]
pub enum MainError {
	IoError(io::Error),
}

#[derive(Parser)]
#[command(version, author, about)]
pub struct Cmd {
	#[arg(long, help = "Set options to values more suitable for hdds (overrides other options)")]
	hdd_mode: bool,

	#[arg(long, help = "List all files before reading them")]
	list_before_reading: bool,

	#[arg(long, help = "File hash cache file", value_name("FILE"))]
	cache_file: Option<String>,

	#[arg(long, help = "How many io threads to use", value_name("THREADS"), default_value_t = 32)]
	io_threads: u64,

	#[arg(
		long,
		help = "How many lister threads to use",
		value_name("THREADS"),
		default_value_t = 64
	)]
	lister_threads: u64,

	#[arg(
		long,
		help = "How many dedupe threads to use",
		value_name("THREADS"),
		default_value_t = 32
	)]
	dedupe_threads: u64,

	#[arg(
		long,
		help = "Minimum file size to consider",
		value_name("BYTES"),
		default_value_t = 4096
	)]
	min_file_size: u64,

	#[arg(
		long,
		help = "Maximum file size to consider",
		value_name("BYTES"),
		default_value_t = u64::MAX,
	)]
	max_file_size: u64,

	#[arg(long, help = "Only hash files partially. Stats will be incorrect.")]
	partial_hash: bool,

	#[arg(long, help = "Force rededuplicate")]
	force_rededup: bool,

	#[arg(
		long,
		requires = "dry_run",
		help = "Allow crossing filesystem boundries. Requires --dry-run"
	)]
	ignore_dev: bool,

	#[arg(long, help = "Don't actually deduplicate")]
	dry_run: bool,

	#[arg(conflicts_with_all = ["out_sizes", "out_sizes_summary"], long, help = "Fdupes-like output")]
	out_fdupes: bool,

	#[arg(long, help = "Output files with sizes (not memory efficient)")]
	out_sizes: bool,

	#[arg(long, help = "Output files with size/count summaries (not memory efficient)")]
	out_sizes_summary: bool,

	#[arg(long, help = "Don't print errors/warnings")]
	quiet: bool,

	#[arg(long, help = "Don't print progress")]
	no_progress: bool,

	#[arg(long, help = "Print more info")]
	verbose: bool,

	#[arg(long, help = "Print more info about threads")]
	thread_verbose: bool,

	#[arg(help = "Directories to begin deduplicating under", value_name("INPUT"), index(1))]
	start_paths: Vec<String>,
}

fn main() {
	// Time measurement stuff
	let start = Instant::now();

	// Parse command line arguments
	let mut cmd = Cmd::parse();

	if cmd.hdd_mode {
		cmd.io_threads = 1;
		cmd.lister_threads = 1;
		cmd.dedupe_threads = 1;
		cmd.list_before_reading = true;
	}

	let settings = &cmd;

	let reading_started = &AtomicBool::new(!settings.list_before_reading);
	let total_working = &AtomicIsize::new(0);

	thread::scope(|scope| {
		// Logs events, metrics, messages, whatever
		let (logger_tx, logger_rx) = unbounded();

		let builder = thread::Builder::new().name("logger".into()).stack_size(THREAD_STACK_SIZE);
		let logger =
			builder.spawn_scoped(scope, || logger::logger(logger_rx, start, settings)).unwrap();

		// Inserts data into a hashmap
		let logger_local_tx = logger_tx.clone();
		let (hashmapper_tx, hashmapper_rx) = unbounded();

		let builder =
			thread::Builder::new().name("hashmapper".into()).stack_size(THREAD_STACK_SIZE);
		let hashmapper = builder
			.spawn_scoped(scope, move || hashmapper(hashmapper_rx, &logger_local_tx))
			.unwrap();

		let read_queue = flume::unbounded();

		let mut precache = None;

		if let Some(cache_file) = &settings.cache_file {
			match File::open(cache_file) {
				Ok(file) => {
					let mut ser_reader = rmp_serde::decode::Deserializer::new(file);
					let mut precache_content = BTreeMap::new();

					while let Ok((fnhash, hash, len)) =
						<(u64, u64, u64)>::deserialize(&mut ser_reader)
					{
						precache_content.insert(fnhash, (hash, len));
					}

					precache = Some(Arc::new(Mutex::new(precache_content)));
				}
				Err(e) => println!("Error opening {cache_file}: {e}",), // TODO: Ignore missing file, better logging
			}
		}

		// Spawn io threads
		let mut ios = vec![];

		for thread_id in 0..settings.io_threads {
			let file_reader = FileReader {
				logger: logger_tx.clone(),
				hashmapper: hashmapper_tx.clone(),
				thread_id,
				read_queue: read_queue.clone(),
				reading_started,
			};

			let builder = thread::Builder::new()
				.name(format!("io_{thread_id}"))
				.stack_size(THREAD_STACK_SIZE);
			let io_thread = builder
				.spawn_scoped(scope, move || {
					file_reader.file_reader_thread(settings);
				})
				.unwrap();

			let io = io_thread;

			ios.push(io);
		}

		// File listing threads
		let list_queue = flume::unbounded();
		let found_filesizes = Arc::new(Mutex::new(HashMap::<u64, Option<FileNameItem>>::new()));
		let mut listers = Vec::new();

		let mut dev = None;

		for start_path in &settings.start_paths {
			// Get the path we will use for initializing the listing
			// Split path into segments, TODO: Probably good idea to implement this for FilePathItem
			let mut last_path = None;

			if start_path == &MAIN_SEPARATOR.to_string() {
				// /
				last_path = Some(Arc::new(FilePathItem::new(OsString::from(""), None)));
			} else {
				for segment in start_path.split(MAIN_SEPARATOR) {
					last_path =
						Some(Arc::new(FilePathItem::new(OsString::from(segment), last_path)));
				}
			}

			let path = last_path.unwrap();

			if !cmd.ignore_dev {
				let metadata = metadata(Path::new(&path.full_path())).unwrap();
				let dev_id = metadata.dev();

				if let Some(current_dev) = dev {
					if current_dev != dev_id {
						panic!("All input paths are not on the same filesystem and --ignore-dev is not specified");
					}
				}

				dev = Some(dev_id);
			}

			// Seed the initial dir for listers
			let (list_send, _) = &list_queue;
			list_send.send(path).unwrap();
		}

		logger_tx.send(logger::Command::ListingStart { time: Instant::now() }).unwrap();
		for thread_id in 0..settings.lister_threads {
			let file_lister = FileLister {
				logger: logger_tx.clone(),
				thread_id,
				dev,
				list_queue: list_queue.clone(),
				total_working,
				reading_started,
				read_queue: read_queue.clone(),
				found_filesizes: found_filesizes.clone(),
				precache: precache.clone(),
				hashmapper: hashmapper_tx.clone(),
			};

			let builder = thread::Builder::new()
				.name(format!("file_lister_{thread_id}"))
				.stack_size(THREAD_STACK_SIZE);
			let file_lister = builder
				.spawn_scoped(scope, move || {
					file_lister.list_files(settings);
				})
				.unwrap();

			listers.push(file_lister);
		}

		drop(hashmapper_tx);

		// Finish up
		for thread in listers {
			thread.join().unwrap();
		}

		logger_tx.send(logger::Command::ListingEnd { time: Instant::now() }).unwrap();

		// Finish the worker threads
		for io in ios {
			io.join().unwrap();
		}

		logger_tx.send(logger::Command::ReadEnd { time: Instant::now() }).unwrap();

		let (onemap, nmap) = hashmapper.join().unwrap();

		// Count the results
		let mut total_hashes = 0;
		let mut hashes_with_duplicates = 0;
		let mut total_duplicate_hashes = 0;
		let mut deduplicate_thread_id = 0;

		let mut stdout = stdout();

		// Spawn dedupe threads
		let mut dedupers = vec![];

		if !settings.dry_run {
			for thread_id in 0..settings.dedupe_threads {
				let (dedupe_tx, dedupe_rx) = unbounded();

				let logger_local_tx = logger_tx.clone();

				let builder = thread::Builder::new()
					.name(format!("dedupe_{thread_id}"))
					.stack_size(THREAD_STACK_SIZE);
				let dedupe_thread = builder
					.spawn_scoped(scope, move || {
						deduplicator(dedupe_rx, &logger_local_tx, thread_id, settings);
					})
					.unwrap();

				let deduper = DeDupeRepr { thread: dedupe_thread, sender: dedupe_tx };

				dedupers.push(deduper);
			}
		}

		total_hashes += onemap.len();

		if let Some(cache_file) = &settings.cache_file {
			match File::create(cache_file) {
				Ok(file) => {
					let mut ser_writer = rmp_serde::encode::Serializer::new(file);

					for (hash, (filename, len)) in onemap {
						let mut fnhasher = SeaHasher::new();
						fnhasher.write(&filename.full_path().into_vec());
						let fnhash = fnhasher.finish();

						(fnhash, hash, len).serialize(&mut ser_writer).unwrap();
					}
					for ((hash, len), values) in &nmap {
						for filename in values {
							let mut fnhasher = SeaHasher::new();
							fnhasher.write(&filename.full_path().into_vec());
							let fnhash = fnhasher.finish();

							(fnhash, hash, len).serialize(&mut ser_writer).unwrap();
						}
					}
				}
				Err(e) => {
					println!("Error opening {cache_file}: {e}");
				}
			}
		}

		let mut sizes = Vec::new();

		for ((_, len), values) in nmap {
			total_hashes += 1;
			hashes_with_duplicates += 1;
			total_duplicate_hashes += values.len();

			if settings.out_sizes || settings.out_sizes_summary {
				let mut items = Vec::new();

				for name in &values {
					items.push((name.full_path(), len));
				}

				sizes.push(((items[0].1 as usize) * values.len(), items));
			}

			if settings.out_fdupes {
				for filename in &values {
					stdout.write_all(filename.full_path().as_bytes()).unwrap();
					println!();
				}
			}

			if !settings.dry_run {
				deduplicate_thread_id += 1;
				deduplicate_thread_id %= settings.dedupe_threads;

				dedupers[deduplicate_thread_id as usize].sender.send((values, len)).unwrap();
			} else {
				for _ in values.iter().skip(1) {
					logger_tx.send(logger::Command::Deduplicated { files: 1, size: len }).unwrap();
				}
			}

			if settings.out_fdupes {
				println!();
			}
		}

		// Finish the worker threads
		for deduper in dedupers {
			drop(deduper.sender);
			deduper.thread.join().unwrap();
		}

		logger_tx.send(logger::Command::DeduplicateEnd { time: Instant::now() }).unwrap();

		logger_tx
			.send(logger::Command::EndReport {
				total_hashes,
				hashes_with_duplicates,
				total_duplicate_hashes,
			})
			.unwrap();

		drop(logger_tx);
		logger.join().unwrap();

		if settings.out_sizes || settings.out_sizes_summary {
			sizes.sort_by(|v, v2| (v.1.len() * v.0).cmp(&(v2.1.len() * v2.0)));

			for (_total_size, items) in sizes {
				if settings.out_sizes_summary {
					println!(
						"{}x {:?}, {:?}",
						items.len(),
						format_size(items[0].1, BINARY),
						items[0].0
					);
				}
				if settings.out_sizes {
					for (name, len) in items {
						println!("{}, {:?}", format_size(len, BINARY), name);
					}
					println!();
				}
			}
		}
	});
}
